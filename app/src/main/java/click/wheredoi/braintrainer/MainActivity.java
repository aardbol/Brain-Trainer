package click.wheredoi.braintrainer;

import android.animation.Animator;
import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

import static java.util.Arrays.asList;

public class MainActivity extends Activity {
    private static final String TAG = MainActivity.class.getName();
    final int gameDuration = 30;
    int gameTimeLeft;
    String correctAnswer;
    int scoreCorrectAnswers = 0;
    int scoreTotalAnswers = 0;
    Random random = new Random();

    // Layout IDs
    TextView tvTime;
    TextView tvScore;
    TextView tvGame;
    TextView tvScoreText;
    Button btnRed;
    Button btnPurple;
    Button btnBlue;
    Button btnGreen;
    Button btnPlayAgain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Register elements
        tvTime = findViewById(R.id.textViewTime);
        tvScore = findViewById(R.id.textViewScore);
        tvGame = findViewById(R.id.textViewGame);
        tvScoreText = findViewById(R.id.textViewScoreText);
        btnRed = findViewById(R.id.buttonRed);
        btnPurple = findViewById(R.id.buttonPurple);
        btnBlue = findViewById(R.id.buttonBlue);
        btnGreen = findViewById(R.id.buttonGreen);
        btnPlayAgain = findViewById(R.id.buttonPlayAgain);
    }

    private void startTimer() {
        new CountDownTimer(gameDuration * 1000 + 100, 1000) {
            @Override
            public void onTick(long l) {
                updateTime((int) l / 1000);
            }

            @Override
            public void onFinish() {
                updateTime(0);
                // Finish game, show results?
                tvScoreText.setText("Your time is up!");
                btnPlayAgain.setVisibility(View.VISIBLE);
                tvScore.setTypeface(null, Typeface.BOLD);
            }
        }.start();
    }

    /**
     * First run of the game. Hides the intro view and shows the game view
     * @param view Button view
     */
    public void startGame(View view) {
        ConstraintLayout clGame = findViewById(R.id.constraintLayoutGame);
        final ConstraintLayout clBegin = findViewById(R.id.constraintLayoutBegin);

        clBegin.animate().alpha(0).setDuration(500).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) { }

            @Override
            public void onAnimationEnd(Animator animator) {
                // Without this the START will only be invisible and still can be clicked
                clBegin.setVisibility(View.GONE);
                startTimer();
            }

            @Override
            public void onAnimationCancel(Animator animator) { }

            @Override
            public void onAnimationRepeat(Animator animator) { }
        });
        clGame.setVisibility(View.VISIBLE);
        newGame();

        // Let the games begin!
        Log.i(TAG, "Game has been started");
    }

    /**
     * Offer a next challenge. This doesn't reset the timer
     */
    private void newGame() {
        int randomCorrectBtn = random.nextInt(3) + 1;
        int firstNumber = random.nextInt(20);
        int secondNumber = random.nextInt(20);
        Log.i(TAG, "Answer calculated: " + (firstNumber + secondNumber));

        correctAnswer = String.valueOf(firstNumber + secondNumber);
        ArrayList<Button> buttons = new ArrayList<>(asList(btnGreen, btnBlue, btnPurple, btnRed));
        Log.i(TAG, "Button idx with correct answer: " + randomCorrectBtn);
        Log.i(TAG, "Answer in String form: " + correctAnswer);

        // One of the four buttons will be chosen randomly to contain the right answer,
        // others are just fakers
        for (int n = 0; n < buttons.size(); n++) {
            if (n != randomCorrectBtn) {
                String newNumber = String.valueOf(random.nextInt(40));
                // The other numbers can't be the same as the correct answer
                while (newNumber.equals(correctAnswer)) {
                    newNumber = String.valueOf(random.nextInt(40));
                }
                buttons.get(n).setText(String.valueOf(newNumber));
                Log.i(TAG, "Fake answer: " + newNumber);
            } else {
                buttons.get(n).setText(correctAnswer);
                Log.i(TAG, "Correct answer is " + correctAnswer);
            }
        }
        tvGame.setText(firstNumber + " + " + secondNumber);
    }

    /**
     * When the game is over, this method can start the game all over again
     * Resets a lot of things.
     */
    public void startNewGame(View view) {
        tvScoreText.setVisibility(View.INVISIBLE);
        tvScore.setTypeface(null, Typeface.NORMAL);
        tvScore.setText("0/0");
        btnPlayAgain.setVisibility(View.GONE);
        scoreCorrectAnswers = 0;
        scoreTotalAnswers = 0;
        startTimer();
        newGame();
        Log.i(TAG, "Another game has been starten");
    }

    public void checkAnswer(View view) {
        if (gameTimeLeft == 0 ) {
            return;
        }
        scoreTotalAnswers++;
        Button answerButton = (Button) view;
        Log.i(TAG, "User guessed " + answerButton.getText().toString());

        if (correctAnswer.equals(answerButton.getText().toString())) {
            scoreCorrectAnswers++;
            tvScoreText.setText(R.string.correct);
        } else {
            tvScoreText.setText(R.string.wrong);
        }
        tvScoreText.setVisibility(View.VISIBLE);
        tvScore.setText(scoreCorrectAnswers + "/" + scoreTotalAnswers);
        newGame();
    }

    private void updateTime(int time) {
        gameTimeLeft = time;
        tvTime.setText(gameTimeLeft + "s");
    }
}
